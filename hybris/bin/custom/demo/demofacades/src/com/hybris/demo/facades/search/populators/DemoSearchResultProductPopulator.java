package com.hybris.demo.facades.search.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.converters.populator.SearchResultProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;

public class DemoSearchResultProductPopulator extends SearchResultProductPopulator {

    @Override
    public void populate(SearchResultValueData source, ProductData target) {
        super.populate(source, target);
        target.setRating(this.getValue(source, "rating"));
        target.setStockStatus(this.getValue(source, "stockStatus"));
    }
}
