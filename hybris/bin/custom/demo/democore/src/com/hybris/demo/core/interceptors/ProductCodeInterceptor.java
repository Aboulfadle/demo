package com.hybris.demo.core.interceptors;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import org.apache.commons.lang.StringUtils;

public class ProductCodeInterceptor implements PrepareInterceptor<ProductModel> {

    private static final String PATTERN = "[[A-Z][a-z]+ [A-Z][a-z]+]{1,10}";

    @Override
    public void onPrepare(ProductModel product, InterceptorContext interceptorContext) throws InterceptorException {
        String code = product.getCode();
        if (StringUtils.isNotEmpty(code) && !code.matches(PATTERN)) {
            throw new InterceptorException("Exception has been occurred");
        }
    }
}
