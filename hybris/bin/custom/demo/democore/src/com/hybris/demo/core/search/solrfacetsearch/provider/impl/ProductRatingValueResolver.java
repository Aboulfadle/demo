package com.hybris.demo.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;

public class ProductRatingValueResolver extends AbstractValueResolver<ProductModel, FeatureList, Object> {

    @Override
    protected void addFieldValues(InputDocument inputDocument, IndexerBatchContext indexerBatchContext, IndexedProperty indexedProperty, ProductModel product, ValueResolverContext<FeatureList, Object> valueResolverContext) throws FieldValueProviderException {
        Float productRating = product.getRating();
        addFieldValue(inputDocument, indexerBatchContext, indexedProperty, productRating, valueResolverContext.getFieldQualifier());
    }

}
