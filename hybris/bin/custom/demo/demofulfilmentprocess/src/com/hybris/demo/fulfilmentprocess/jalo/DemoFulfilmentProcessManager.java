/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.hybris.demo.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.hybris.demo.fulfilmentprocess.constants.DemoFulfilmentProcessConstants;

public class DemoFulfilmentProcessManager extends GeneratedDemoFulfilmentProcessManager
{
	public static final DemoFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (DemoFulfilmentProcessManager) em.getExtension(DemoFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
