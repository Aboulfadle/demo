package com.hybris.demo.storefront.controllers.cms;

import com.hybris.demo.core.model.CMSYoutubeComponentModel;
import com.hybris.demo.storefront.controllers.ControllerConstants;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller("CMSYoutubeComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.CMSYoutubeComponent)
public class CMSYoutubeComponentController extends AbstractAcceleratorCMSComponentController<CMSYoutubeComponentModel> {

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model, final CMSYoutubeComponentModel component) {
        model.addAttribute("mute", 1);
        model.addAttribute("autoPlay", 1);
    }
}
