package com.hybris.demo.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/medias")
public class MediaPageController extends AbstractPageController {
    private static final String MEDIA_PAGE_LABEL = "grid-page";

    @RequestMapping(method = RequestMethod.GET)
    public String getAllowancesPage(final Model model) throws CMSItemNotFoundException {
        final ContentPageModel trainingPage = getContentPageForLabelOrId(MEDIA_PAGE_LABEL);
        storeCmsPageInModel(model, trainingPage);
        setUpMetaDataForContentPage(model, trainingPage);
        return getViewForPage(model);
    }
}
