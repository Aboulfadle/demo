<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<template:page pageTitle="${pageTitle}">

    <div class="row training">
        <cms:pageSlot position="header" var="feature">
            <cms:component component="${feature}" element="div" class="col-12"/>
        </cms:pageSlot>
    </div>

    <div class="row training">
        <cms:pageSlot position="navigationBar" var="feature">
            <cms:component component="${feature}" element="div" class="col-12"/>
        </cms:pageSlot>
    </div>

    <div class="row training">
        <cms:pageSlot position="section1" var="feature">
            <cms:component component="${feature}" element="div" class="col-6"/>
        </cms:pageSlot>

    </div>

    <div class="row training">
        <cms:pageSlot position="section2" var="feature">
            <cms:component component="${feature}" element="div" class="col-6"/>
        </cms:pageSlot>
    </div>

    <div class="row training">
        <cms:pageSlot position="section3" var="feature">
            <cms:component component="${feature}" element="div" class="col-12"/>
        </cms:pageSlot>
    </div>

    <div class="row training">
        <cms:pageSlot position="footer" var="feature">
            <cms:component component="${feature}" element="div" class="col-12"/>
        </cms:pageSlot>
    </div>

</template:page>
