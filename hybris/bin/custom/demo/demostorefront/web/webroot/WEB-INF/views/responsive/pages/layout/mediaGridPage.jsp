<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">

    <cms:pageSlot position="headerPosition" var="feature">
        <cms:component component="${feature}" element="div" class="col-12"/>
    </cms:pageSlot>


    <cms:pageSlot position="gridPosition" var="feature" element="div">
        <cms:component component="${feature}" element="div"/>
    </cms:pageSlot>

    <cms:pageSlot position="footerPosition" var="feature" element="div">
        <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
    </cms:pageSlot>



</template:page>
