<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="mediaYoutube" tagdir="/WEB-INF/tags/responsive/media" %>

<h2>${component.title}</h2>
<c:forEach var="item" items="${component.items}">
    <mediaYoutube:mediaItem item="${item}"/>
</c:forEach>

