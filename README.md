# AUBAY Training - SAP Commerce cloud 1905

**Requirements :**

JDK 11 or later

Node 12.16.0 is recommended


**Steps to set up a local instance :**

1. Create the root directory e.g C:\aubay

1. Open a git bach terminal and clone the project `git clone https://gitlab.com/Aboulfadle/demo.git`

1. Download the hybris commerce suite 1905.5 https://drive.google.com/file/d/1wFzV4xmj1K1ag-4dVNXrMI7dkVFtceoG/view?usp=sharing

1. Put the modules and the plateform folder inside the <HYBRIS_HOME_DIR>/bin

1. Download the data.zip https://drive.google.com/file/d/1cbhXHbz9ARb-rPWQW3_FdVCTXjkxDyao/view?usp=sharing

1. Extract the data folder inside <HYBRIS_HOME_DIR>

1. Open a command line and navigate to the <HYBRIS_BIN_DIR>/platform directory and set your ant environment by running `setantenv.bat`

1. Install smarteditaddon `ant addoninstall -Daddonnames=smarteditaddon -DaddonStorefront.yacceleratorstorefront=demostorefront`

1. Run `ant clean all` and `ant updatesystem` from the <HYBRIS_BIN_DIR>/platform directory.

1. Download hybris demo/dev free licence https://drive.google.com/file/d/11u9qg7AEcvOMc7PF7Hm4iPQ7rv2ifQrd/view?usp=sharing

1. Extract/Overwrite hybrislicence.zip inside <HYBRIS_BIN_DIR>/config/licence

1. Add the following line to your hosts file : 127.0.0.1   localhost electronics.local apparel-uk.local apparel-de.local

1. Start the hybris server by running the following command from the <HYBRIS_BIN_DIR>/platform folder `hybrisserver.bat`

1. You can access the B2C Accelerator electronics storefront with the following URL https://electronics.local:9002/demostorefront/

1. You can access the backoffice with this URL https://localhost:9002/backoffice/

1. You can access the HAC with this URL https://localhost:9002/

1. You can access the smartedit with this URL https://localhost:9002/smartedit/

1. You can access the SOLR dashboard with this URL https://localhost:8983/solr/


**Import your project :**

In order to import hybris project in Intellij, you'll need to install `SAP Commerce Developers Toolset` plugin.

Then click on `File > New > Projects from existing sources` and select the bin directory of your hybris project.

In the dialog popup, click on `import project from external model` radio button. Then select `Hybris`.

Click on `Next` couple of times. Then it will display all the extensions you'll need to import for your project.
